HTTP API
========

Live documentation
------------------

Full, interactive Swagger documentation for your microscopes web API is available from the microscope itself. From any browser, go to ``http://{your microscope IP address}/api/v2/docs/swagger-ui``.

.. note:: We should have an online copy of the API SwaggerUI documentation up soon.