# Feature request

## Summary

(Summarize the bug encountered concisely)

## Configuration

I'm using:

**Camera:** (E.g. Raspberry Pi camera v2)

**Motor controller:** (E.g. Sangaboard, or Arduino Nano)

## Additional details

(Anything else you think might be relevant to mention)

/label ~feature
