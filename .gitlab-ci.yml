stages:
  - analysis
  - testing
  - build
  - package
  - deploy

# Re-usable block to install (and cache) Poetry and openflexure-microscope-server
.poetry-install-template: &poetry-install
  before_script:
    - curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
    - source $HOME/.poetry/env
    - poetry config virtualenvs.in-project true
    - poetry install -vv
  cache:
    key: "${CI_COMMIT_REF_SLUG}"
    paths:
      - .cache/pip
      - .venv

# Re-usable block to install (and cache) openflexure-microscope-server static app
.node-install-template: &node-install
  before_script:
    - cd openflexure_microscope/api/static/
    - npm install
  cache:
    key: "${CI_COMMIT_REF_SLUG}"
    paths:
      - openflexure_microscope/api/static/node_modules

# Python static analysis with PyLint
pylint:
  stage: analysis
  image: python:3.7
  retry: 1

  <<: *poetry-install

  script:
    - poetry run poe pylint

  only:
    - branches
    - merge_requests
    - tags
    - web

# Python type checking with Mypy
mypy:
  stage: analysis
  image: python:3.7
  retry: 1

  <<: *poetry-install

  script:
    - poetry run poe mypy

  artifacts:
    reports:
      cobertura: openflexure_microscope/cobertura.xml

  only:
    - branches
    - merge_requests
    - tags
    - web

# Python style analysis with Black
black:
  stage: analysis
  image: python:3.7
  retry: 1
  allow_failure: true

  <<: *poetry-install

  script:
    # Run static build script
    - poetry run poe black_check

  only:
    - branches
    - merge_requests
    - tags
    - web

# Python unit tests with PyTest
pytest:
  stage: testing
  image: python:3.7
  retry: 1

  <<: *poetry-install

  script:
    - poetry run poe test

  only:
    - branches
    - merge_requests
    - tags
    - web

# JavaScript linting with ESLint (via Vue CLI)
eslint:
  stage: analysis
  image: node:15

  <<: *node-install

  script:
    # Build JS application for production
    - npm run lint

  only:
    - branches
    - merge_requests
    - tags
    - web

# Build JS app
build:
  stage: build
  image: node:15

  <<: *node-install

  script:
    # Build JS application for production
    - npm run build

  artifacts:
    name: "dist"
    expire_in: 1 week
    paths:
      - "./openflexure_microscope/api/static/dist/"

  only:
    - merge_requests
    - tags
    - web

# Package application into distribution tarball
package:
  stage: package
  dependencies:
    - build

  image: ubuntu:latest

  script:
    # Build distribution archive
    - mkdir -p dist
    - tar -c --exclude-vcs --exclude-from .tarignore -vzf dist/openflexure-microscope-server-${CI_COMMIT_REF_NAME}.tar.gz .
    - cd dist/
    - sha256sum openflexure-microscope-server-${CI_COMMIT_REF_NAME}.tar.gz > openflexure-microscope-server-${CI_COMMIT_REF_NAME}.tar.gz.sha256

  artifacts:
    name: "dist"
    expire_in: 1 week
    paths:
      - "./dist/openflexure-microscope-server-${CI_COMMIT_REF_NAME}.tar.gz"
      - "./dist/openflexure-microscope-server-${CI_COMMIT_REF_NAME}.tar.gz.sha256"

  only:
    - merge_requests
    - tags
    - web

# Deploy to builds.openflexure.org
deploy:
  stage: deploy
  dependencies:
    - package

  image: ubuntu:latest

  before_script:
    - "which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )"
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$SSH_PRIVATE_KEY_BATH_OPENFLEXURE_BASE64" | base64 --decode)
    - mkdir -p ~/.ssh
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config

  script:
    # Install rsync if not already installed
    - "which rsync || ( apt-get update -y && apt-get install rsync -y )"

    # Upload the builds folder to openflexure-microscope builds
    - rsync -hrvz -e ssh dist/ ci-user@openflexure.bath.ac.uk:/var/www/build/openflexure-microscope-server

    # Run update-latest.py on the build server
    - ssh -t ci-user@openflexure.bath.ac.uk "/var/www/build/update-latest.py"

  only:
    - tags
    - web
